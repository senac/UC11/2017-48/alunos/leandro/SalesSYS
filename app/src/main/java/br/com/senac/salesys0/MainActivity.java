package br.com.senac.salesys0;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String CLIENTE = "cliente" ;
    public static final int NOVO = 1  ;
    private List<cliente> listaCliente = new ArrayList<>();
    private ArrayAdapter<cliente> adaptador ;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listaCliente.add(new cliente("fer","","","","","","",""));
        listaCliente.add(new cliente("leo","","","","","","","")) ;
        listaCliente.add(new cliente("abc","","","","","","","") );

        ListView listView = findViewById(R.id.listaCliente) ;

        adaptador = new ArrayAdapter<cliente>(this
                ,android.R.layout.simple_list_item_1
                , listaCliente) ;

        listView.setAdapter(adaptador);

        


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posicao, long posicaoItemAdapter) {

                cliente cliente  = adaptador.getItem(posicao) ;
                Intent intent = new Intent(MainActivity.this , resultado.class) ;
                intent.putExtra(CLIENTE , cliente) ;
                startActivityForResult(intent , 02);

            }
        });


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.novo :  Intent intent = new Intent(MainActivity.this, resultado.class);
                startActivityForResult(intent, NOVO);      break;

            case R.id.sobre : break;

        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == NOVO){

            if(resultCode == RESULT_OK){
                cliente cliente = (cliente) data.getSerializableExtra(resultado.CLIENTE) ;
                listaCliente.add(cliente);
                adaptador.notifyDataSetChanged();
                Toast.makeText(this , "Nome:"+ listaCliente.size() , Toast.LENGTH_LONG).show();
            }else if(resultCode == RESULT_CANCELED){ }


        }


    }



}



